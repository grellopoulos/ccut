# ccut
ccut.py works pretty much like Linux/UNIX standard cut, but also counts the chosen field/field combination of a CSV-like file. Use it, if you want to analize a CSV-like file that contains file paths and file names with extensions in order to count mimetypes, extensions etc. Have a look at the documentation to learn more about it!
